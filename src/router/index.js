import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Index from '@/pages/index'
import About from '@/pages/about'
import Contact from '@/pages/contact'
import News from '@/pages/news'
import Product from '@/pages/product'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/news',
      name: 'news',
      component: News
    },
    {
      path: '/product',
      name: 'product',
      component: Product
    }
  ]
})
